import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch, useSelector } from 'react-redux';
import {getCalcPoints} from '../store/actions/points';



export default ({ name, today, title, idUser }) => {

    
    const date = new Date();
    const date2 = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`
    
    const { point } = useSelector(state => state.points);
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch(getCalcPoints(idUser , '2022-05-25'))
    }, [])

    return (

        <LinearGradient colors={['#134E5E', '#71B280']} style={styles.container}>
            <View style={styles.title}>
                <View><Text style={[styles.titleName, styles.primaryColor]}>{name ? "Olá" : ""} {name || title}</Text></View>
                {today}
            </View>
            {name ? <View style={styles.hours}>
                <View>
                    <Text style={[styles.primaryColor, {fontSize: 16}]}>Horas Trabalhadas</Text>
                    <Text style={[styles.primaryColor, styles.hoursNumber]}>{point}</Text>
                </View>
                <View>
                    <Text style={[styles.primaryColor, {fontSize: 16}]}>Horas Total</Text>
                    <Text style={[styles.primaryColor, styles.hoursNumber]}>08:00</Text>
                </View>


            </View> : null


            }

        </LinearGradient>




    )
}

const styles = StyleSheet.create(
    {
        container: {
            flexGrow: 0.3,
            backgroundColor: "#00B528",
            justifyContent: "space-around",
        },
        title: {
            alignItems: "center",
            marginTop: 50
        },
        titleName: {
            fontSize: 28,
            fontWeight: "600",
            marginBottom: 20
        },
        hours: {
            flexDirection: "row",
            justifyContent: "space-around",


        },
        hoursNumber: {
            fontSize: 21,
            fontWeight: '500',
            marginTop: 10
        },

        primaryColor: {
            color: "#fff"
        },


    }
)