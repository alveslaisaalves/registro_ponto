import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Alert} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDispatch } from "react-redux";
import { logout } from "../store/actions/user";
import { useNavigation } from '@react-navigation/native';

export default ({color}) => {

    const dispatch = useDispatch();
    const navigation = useNavigation();

    const onLogout = () => {
        dispatch(logout())
        navigation.navigate("Login")
    }
    return (
        <TouchableOpacity onPress={onLogout} >
            <View>
                <Icon name="logout" size={30} color="red" />
            </View>
        </TouchableOpacity>
    )
}
// const styles = StyleSheet.create({
//     container: {
//         width: 60,
//         height: 60,
//         borderRadius: 30,
//         alignItems: 'center',
//         justifyContent: 'center',
//         marginBottom: 30,
//         backgroundColor: "#00B528"
        
//     }
// })