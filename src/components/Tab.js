import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import Profile from '../screens/Profile';



export default () => {
    const Tab = createBottomTabNavigator();
    return (
        <Tab.Navigator tabBarOptions={{
            activeTintColor: "#00B528",
            inactiveTintColor: "#00B522",
            labelStyle: {fontSize: 24}
        }}>
            <Tab.Screen name="Profile" component={Profile} />
        </Tab.Navigator>
    );
}