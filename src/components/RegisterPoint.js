import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    Modal,
    TextInput,
    TouchableOpacity
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { getListPoints } from "../store/actions/points";
import { appStyle } from "../styles";
import { showModal, hideModal } from "../store/actions/modal";

export default () => {

    const date = new Date();
    const date2 = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
    const time = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`


    const { user } = useSelector(state => state.user);
    const { listPoints } = useSelector(state => state.points);
    const { modalVisible } = useSelector(state => state.modalVisible);
    const dispatch = useDispatch();
   
    const [justification, setJustification] = useState("Entrada");

    useEffect(() => {
        dispatch(getListPoints(user.userId, date2));
    }, [])


    const ButtonModal = ({ title, color, onPress}) => (
        <TouchableOpacity onPress={onPress} style={[styles.modalButton, { backgroundColor: color }]}>
            <Text style={styles.modalButtonText}>{title}</Text>
        </TouchableOpacity>
    );

    const renderItem = (point) => {

        return <View>
            <View style={styles.container}>
                <Text style={styles.hour}>{point.item.dateTimeRegistro}</Text>

                <Text style={styles.entry}>{point.item.justification}</Text>
            </View>

            <View style={{ height: 0.5, backgroundColor: 'grey', width: '80%', justifyContent: 'space-between' }} />
        </View>



    }

    const cancelButton = () => dispatch(hideModal());

    const register = () => {

    }
    
    return (
        <View>
            <FlatList data={listPoints}
                keyExtractor={listPoints => listPoints.dateTimeRegistro}
                renderItem={renderItem}
            />

            <Modal
                animationType='slide'
                visible={modalVisible}
                transparent={true}
            >
                <View style={styles.modal}>
                    <Text style={styles.titleRegisterPoint}>Registrar Ponto</Text>
                    <Text style={styles.textRegisterPoint}>Ponto: {date2} {time}</Text>
                    <View>
                        <TextInput style={styles.input} value={justification} onChangeText={setJustification} />
                    </View>
                    <View style={styles.modalButtons}>
                        <ButtonModal title="Registrar" color={appStyle.secundaryColor} />
                        <ButtonModal onPress={cancelButton} title="Cancelar" color="grey" />
                       
                    </View>
                </View>

            </Modal>
        </View>
    )


}
const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        width: '60%',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 30
    },
    hour: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    entry: {
        fontSize: 20,
        fontWeight: 'semi-bold',
        marginLeft: 20
    },
    modal: {
        backgroundColor: appStyle.primaryColor,
        margin: 20,
        padding: 20,
        borderRadius: 10,
        elevation: 50,
        top: '50%'
    },
    modalButtons: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingTop: 30

    },
    modalButton: {

        height: 40,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        width: 160


    },
    modalButtonText: {
        color: appStyle.primaryColor,

    },
    titleRegisterPoint: {
        fontSize: 20,
        fontWeight: '600',
        marginBottom: 10
    },
    textRegisterPoint: {
        fontSize: 18
    },
    input: {
        fontSize: 18,
        borderWidth: 1,
        marginTop: 20,
        paddingLeft: 10
    }
})