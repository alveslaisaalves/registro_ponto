import { CurrentRenderContext } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet } from "react-native"


export default ({dateType}) => {
    const [date, setDate] = useState(null);
    const dayName = new Array("domingo", "segunda feira", "terça feira", "quarta feira", "quinta feira", "sexta feira", "sábado")
    const monName = new Array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro")
    useEffect(() => {
        const today = new Date();
        const monthDate = dayName[today.getDay()] + ", " + today.getDate() + " de " + monName[today.getMonth()];
        const fullDate= monName[today.getMonth()]+"/"+today.getFullYear();

        if(dateType === "month"){
            setDate(monthDate);
        }else if(dateType ==="year"){
            setDate(fullDate);
        }
        
    }, []);

    return (
        <View >
            
                <Text style={[styles.titleDate, styles.primaryColor]}>{date}</Text>
            
          
        </View>

    )
}

const styles = StyleSheet.create(
    {
       
        title: {
            alignItems: "center",
            
        },
        titleDate: {
            fontSize: 18
        },

        primaryColor: {
            color: "#fff"
        }
    }
)