import _interopRequireDefault from "@babel/runtime/helpers/interopRequireDefault";
import React from "react";
import { View, Text, StyleSheet, TextInput } from "react-native";

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default ({name, value, onChangeText, label, keyboardType, placeholder, secure, autoFocus, isValid }) => {
    return (
        <View style={input.container}>
            <Text style={[input.label, isValid ? {color: "green"} : {color: "red"}]}>{label}</Text>
            <TextInput style={input.style}
                name={name}
                value={value}
                onChangeText={onChangeText}
                keyboardType={keyboardType}
                placeholder = {placeholder}
                secureTextEntry={secure} 
                autoFocus={autoFocus}/>
               
        </View>
    )
}

const input = StyleSheet.create({
    container: {
        borderRadius: 5,
        justifyContent: "center",
        paddingLeft: 10,
        borderBottomColor: "#979797",
        borderWidth: 1,
        width: 325,
        height: 58,
        marginTop: 10,
        marginBottom: 14
    },
    style: {
        marginBottom: 15,
        justifyContent: "center",
        fontSize: 18,

    },
    label: {
        fontSize: 12,
        paddingTop: 20,
       
    }
})