import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default ({text, colorFont}) => {
    return (
        <View style={styles.containerText}>
            <Text style={[colorFont ? {color: colorFont } : {color: "red"},styles.text]}>{text}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    containerText: {
        marginTop: 10,
        marginBottom: 20,
        padding: 10,
        width: '80%',
        borderRadius: 5,
   
    },
    text: {
        fontSize: 17,
        fontWeight: '600',
        textAlign: 'center'
        
    }

});