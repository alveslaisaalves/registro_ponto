import React from "react";
import {View, Text, StyleSheet, TouchableOpacity, Alert} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { appStyle } from "../styles";
import { showModal } from "../store/actions/modal";
import { useDispatch, useSelector } from "react-redux";

export default ({color, route }) => {

    const dispatch = useDispatch();

    return (
       
        <TouchableOpacity style={styles.container} onPress={()=> dispatch(showModal())} >
             <LinearGradient colors={['#134E5E', '#71B280']} style={styles.container}>
                <View>
                    <Icon name="alarm" size={40} color={"#fff"} />
                </View>
            </LinearGradient>
        </TouchableOpacity>

    )
}
const styles = StyleSheet.create({
    container: {
        width: 60,
        height: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 30,
        // backgroundColor: "#71B354"
        
    }
})