import { SHOW_MODAL, HIDE_MODAL } from "../actions/actionTypes";

const initialState = {
    modalVisible: false,

}

export const modalVisible = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_MODAL:
            return {
                ...state,
                modalVisible: true,
            };
        case HIDE_MODAL:
            return {
                ...state,
                modalVisible: false,
            };
        default:
            return state;

    }
}

