import { SET_MESSAGE, CLEAR_MESSAGE } from "../actions/actionTypes";

const initialState = {
  text: ''
};
export const message =  (state = initialState, action) => {
 
  switch (action.type) {
    case SET_MESSAGE:
      return { 
        ...state,
        text: action.payload.text
      };
    case CLEAR_MESSAGE:
      return { text: ''};
    default:
      return state;
  }
}