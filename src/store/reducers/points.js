import AsyncStorage from "@react-native-community/async-storage";
import { CALC_POINTS, LIST_POINTS} from "../actions/actionTypes";


// const userAsync = AsyncStorage.getItem("@user");
// const initialState = user ? { isLoggedIn: true, user } : { isLoggedIn: false, user: null }
const initialState = {
    userId: null,
    point: null,
    listPoints: null

}

export const points = (state = initialState, action) => {
    switch (action.type) {
        case CALC_POINTS:
            return {
                ...state,
                point: action.payload.calc,
            };
            case LIST_POINTS:
                return {
                    ...state,
                    listPoints: action.payload.listPoints
                }
           
        default:
            return state;
        
    }
}

