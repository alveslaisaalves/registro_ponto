import AsyncStorage from "@react-native-community/async-storage";
import { LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT, REGISTER_SUCCESS, REGISTER_FAIL, UPDATE_USER } from "../actions/actionTypes";


const userAsync = AsyncStorage.getItem("@user");
// const initialState = user ? { isLoggedIn: true, user } : { isLoggedIn: false, user: null }
const initialState = {
    isLoggedIn: false,
    user: null,

}

export const user = (state = initialState, action) => {
    switch (action.type) {
        case REGISTER_SUCCESS:
            return {
                ...state,
                isLoggedIn: false,
           
            };
        case REGISTER_FAIL:
            return {
                ...state,
                isLoggedIn: false,
         
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isLoggedIn: true,
                user: action.payload.user,

            };
        case LOGIN_FAIL:
            return {
                ...state,
                isLoggedIn: false,
                user: null,
            };
        case LOGOUT:
            return {
                ...state,
                isLoggedIn: false,
                user: null,
            };
    
        case UPDATE_USER:
            return {
                ...state,
          
            };
           
        default:
            return state;
        
    }
}

