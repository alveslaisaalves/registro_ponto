import { combineReducers } from 'redux';
import {user} from './user';
import {message} from './message';
import {points} from './points';
import { modalVisible } from './modal';

export default combineReducers({
    user,
    message,
    points,
    modalVisible
})