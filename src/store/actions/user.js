import { REGISTER_FAIL, 
  REGISTER_SUCCESS, 
  LOGIN_FAIL, 
  LOGIN_SUCCESS, 
  LOGOUT, 
  UPDATE_USER, 
  SET_MESSAGE } from "./actionTypes";
import userService from "../../services/user.service";



export const register = (username, email, password) => dispatch => {
  return userService.register(username, email, password).then(
    (response) => {

      dispatch({ type: REGISTER_SUCCESS });
      dispatch({
        type: SET_MESSAGE,
        payload: { text: "Usuario cadastrado com sucesso" }
      })
     
      return Promise.resolve();
    }
    , error => {
      dispatch({ type: REGISTER_FAIL });
      dispatch({
        type: SET_MESSAGE,
        payload: {text: "Não foi possivel efetuar o cadastro"}
      })
      return Promise.reject();
    });
};

export const login = (email, password) => (dispatch) => {

  return userService.login(email, password).then(
    response => {
      dispatch({
        type: LOGIN_SUCCESS,
        payload: { user: response },
      });
      
      return Promise.resolve();
    },
    error => {
      dispatch({
        type: LOGIN_FAIL
      });
      let message = "";

      if (error.message === 'Network Error') {
        message = "Falha de conexão";
      }else if(error.response.status === 404) {
         message = "Senha ou e-mail incorreto";
      }

      dispatch({
        type: SET_MESSAGE,
        payload: { text: message }
      })
      return Promise.reject();
    }
  );

};


export const logout = () => dispatch => {
  userService.logout();
  dispatch({
    type: LOGOUT
  })
}

export const update = (id, name, email, password) => dispatch => {
  return userService.update(id, name, email, password).then(
    response => {
      dispatch({ type: UPDATE_USER, payload: { user: response } });
      dispatch({
        type: SET_MESSAGE,
        payload: {text: "Atualização concluida com sucesso"}
      })
      return Promise.resolve();

    }
  ).catch(error => {
    dispatch({
      type: SET_MESSAGE,
      payload: {text: "Não foi possivel atualizar"}
    })
    return Promise.reject();

  })
}


