import { SHOW_MODAL, HIDE_MODAL } from "./actionTypes";

export const showModal = (value) => dispatch => dispatch({type: SHOW_MODAL});
export const hideModal =(value) => dispatch => dispatch({type: HIDE_MODAL})