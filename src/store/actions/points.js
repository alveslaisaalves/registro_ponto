import { CALC_POINTS, LIST_POINTS } from "./actionTypes";
import checkPointService from "../../services/checkPoint.service";


export const getCalcPoints = (idUser, date) => dispatch => {
  return checkPointService.getCalcPoints(idUser, date).then(
    (response) => {
      dispatch({ type: CALC_POINTS, payload: {calc:response} })
    
      return Promise.resolve();
    }
    , error => {
      return Promise.reject();
    });
};

export const getListPoints = (idUser, date) => dispatch => {
  return checkPointService.getListPoints(idUser, date).then(
    (response) => {
      dispatch({ type: LIST_POINTS, payload: {listPoints:response} })
      console.log(response)
      return Promise.resolve();
    }
    , error => {
      return Promise.reject();
    });
};