import { CLEAR_MESSAGE } from "./actionTypes";

export const clearMessage = () => dispatch => dispatch({type: CLEAR_MESSAGE});
