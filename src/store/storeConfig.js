import {createStore, compose, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers/index"

const storeConfig = () => {
  return createStore(rootReducer, applyMiddleware(thunk))
}

export default storeConfig;

