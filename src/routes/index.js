import React, { useState } from "react";
import {useSelector} from "react-redux"
import AuthRoutes from "./auth.routes";
import AppRoutes from "./app.routes";

const Routes = (props) => {  
    const { isLoggedIn } = useSelector(state => state.user)
    const routes = isLoggedIn ? <AppRoutes /> : <AuthRoutes />;
    return routes 
}

export default Routes;