import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Profile from '../screens/Profile';
import Home from '../screens/Home';
import Calendar from '../screens/Calendar'
import RegisterPointButton from '../components/RegisterButton'
import Logout from '../components/Logout'
import { appStyle } from '../styles';



export default () => {

    const Tab = createBottomTabNavigator();
    return (
        <Tab.Navigator 
            screenOptions={
                { headerShown: false, tabBarShowLabel: false }
            
               
            }
            tabBarOptions={{
                activeTintColor: appStyle.secundaryColor,
                inactiveTintColor: "#71B280",

                style: {
                    backgroundColor: "#fff",

                },

                tabStyle: {
                    paddingBottom: 4,
                    paddingTop: 5,

                },


            }} >
            
             <Tab.Screen name="Home"
                component={Home}
                options={{
                    tabBarIcon: ({ size, color }) => (<Icon name="home" size={30} color={color} />)
                }} /> 

            <Tab.Screen name="Calendar"
                component={Calendar}
                options={{

                    tabBarIcon: ({ size, color }) => (<Icon name="calendar" size={30} color={color} />)
                }} />

            <Tab.Screen name="Ponto"
                component={Home}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ size, color}) => (
                       
                        <RegisterPointButton color={color} />
                    
                    )}} />
            <Tab.Screen name="Perfil"
                component={Profile}
                options={{

                    tabBarIcon: ({ size, color }) => (<Icon name="account" size={30} color={color} />)
                }} />
            
            <Tab.Screen name="Sair"
                component={Profile}
                options={{
                    tabBarIcon: ({color}) => (<Logout color={color}/>)
                }} />
        </Tab.Navigator>
    );
}

























// import React from 'react';
// import { createNativeStackNavigator } from '@react-navigation/native-stack';
// import { NavigationContainer } from '@react-navigation/native';
// import User from "../screens/User";
// import Perfil from '../screens/Profile';

// export default () => {
//     const AppStack = createNativeStackNavigator();
//     return (
//         <NavigationContainer>
//             <AppStack.Navigator initialRouteName='Home' screenOptions={{ headerShown: false}}>
//                 <AppStack.Screen name="Home" component={User} />
//                 <AppStack.Screen name="Perfil" component={Perfil} /> 
//             </AppStack.Navigator>
//         </NavigationContainer>

//     );
// }