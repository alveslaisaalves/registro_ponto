import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Register from "../screens/Register.js";
import Login from "../screens/Login.js";

export default () => {
    const AuthStack = createNativeStackNavigator();

    return (
        <AuthStack.Navigator initialRouteName="Login" screenOptions={{ headerShown: false}}>
            <AuthStack.Screen name="Login" component={Login} />
            <AuthStack.Screen name="SignUp" component={Register} /> 
        </AuthStack.Navigator> 
    );
}