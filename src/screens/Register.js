import React, { useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    Button,
    TouchableOpacity,
    ScrollView
} from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Input from '../components/Input';
import { appStyle, container } from '../styles';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from "react-redux";
import { register } from '../store/actions/user';
import Message from "../components/Message";


const SignUp = () => {
    const navigation = useNavigation();
    const [name, setName] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [confirmPassword, setConfirmPassword] = useState();
    const [sucessful, setSucessful] = useState(true);
    const dispatch = useDispatch();

    const { text } = useSelector(state => state.message);

    const validations = [];
    validations.push(name && name.trim());
    validations.push(email && email.includes('@'))

    validations.push(password && password.length >= 4);
    validations.push(confirmPassword);
    validations.push(password == confirmPassword);
    const [nameValid, emailValid, passwordValid, confirmPasswordValid, comparePassword] = validations;
    const validForm = validations.reduce((all, v) => all && v)

    const handlerRegister = () => {
        dispatch(register(name, email, password)).then(
            () => setSucessful(true)
        ).catch(
            () => setSucessful(false)
        )
    }

    return (
        <ScrollView>
            <View style={container.style}>
                <View style={styles.titleContainer}>
                    <Icon name="alarm" size={80} color="#00B528" />
                    <Text style={styles.title}>Cadastro</Text>
                </View>
                {text && text.trim() ? ( sucessful ? <Message text={text} colorFont={appStyle.mossColor}/> : <Message text={text} colorFont="red"/>  ) : null}
                <View style={styles.inputs}>
                    <Input label="Nome" value={name} autoFocus={true} onChangeText={name => setName(name)} placeholder="Nome" isValid={nameValid} />
                    <Input label="E-mail" value={email} onChangeText={email => setEmail(email)} keyboardType="email-address" placeholder="Email" isValid={emailValid} />
                    {/* <Input label="Telefone" keyboardType="phone-pad" placeholder="Telefone"/> */}
                    <Input label="Senha" value={password} onChangeText={password => setPassword(password)} secure={true} keyboardType="visible-password" placeholder="Senha" isValid={passwordValid} />
                    <Input label="Confirmacao de Senha" value={confirmPassword} onChangeText={confirmPassword => setConfirmPassword(confirmPassword)}
                        isValid={confirmPasswordValid && comparePassword}
                        secure={true} keyboardType="visible-password" placeholder="Senha " />
                </View>
                <View style={styles.containerRegister}>
                    <Text style={styles.registerText}>Já tem conta?</Text>
                    <TouchableOpacity onPress={() => navigation.navigate("Login")}><Text
                        style={styles.register}>Entrar</Text></TouchableOpacity>
                </View>
              
          
                <Button
                    style={[!validForm ? { backgroundColor: "#AAA" } : { backgroundColor: "#00B528" }]}
                    disabled={!validForm}
                    onPress={handlerRegister} title="Cadastrar" />

            </View>
        </ScrollView>

    )
}

const styles = StyleSheet.create({
    inputs: {
        display: 'flex',
        marginTop: 30,
        marginBottom: 10

    },
    title: {
        fontSize: 40,
        fontWeight: '700',

    },
    titleContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 60,
    },
    containerRegister: {
        flexDirection: 'row',
        marginBottom: 10
    },
    register: {
        fontSize: 15,
        color: '#00B528',
        fontWeight: '500',

    },
    registerText: {
        fontSize: 15,
        marginRight: 7
    },
    button: {
        backgroundColor: '#00B528'
    }
})

export default SignUp;
