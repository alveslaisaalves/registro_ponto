import React, { useState } from "react";
import { View, Text, StyleSheet, Button, TextInput, TouchableOpacity } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { update } from "../store/actions/user";
import Header from "../components/Header";
import { appStyle } from "../styles";
import Message from "../components/Message";

export default () => {
  const { user } = useSelector(state => state.user);
  const { text } = useSelector(state => state.message);
  const dispatch = useDispatch();
  const [name, onChangeName] = useState(user.name);
  const [email, onChangeEmail] = useState(user.email);
  const [password, onChangePassword] = useState("******");

  const userUpdate = () => {
    dispatch(update(user.userId, name, email, password))
  }
  return (
    <View style={styles.container}>
      <Header title="Perfil" />

      <View style={styles.inputContainer}>
        <View style={styles.box}>
          <Text style={styles.label}>Nome:</Text>
          <TextInput style={styles.input} value={name} onChangeText={onChangeName} />
        </View>
        <View style={styles.box}>
          <Text style={styles.label}>Email:</Text>
          <TextInput style={styles.input} value={email} onChangeText={onChangeEmail} />
        </View>
        <View style={styles.box}>
          <Text style={styles.label}>Senha:</Text>
          <TextInput style={styles.input} secureTextEntry={true} value={password} onChangeText={onChangePassword} />
        </View>
      </View>
      <View style={styles.message}>
        {text ? <Message text={text} colorFont={appStyle.mossColor}/> : null}
      </View>
      
      <View style={styles.containerButton}>
        <TouchableOpacity onPress={userUpdate}
          style={styles.button}
          accessibilityLabel="Learn more about this green button"
        >

          <Text style={styles.textButton}>ATUALIZAR</Text>
        </TouchableOpacity>
      </View>


    </View>

  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',

  },
  header: {
    flexGrow: 0.3,
    backgroundColor: '#00B528',
    justifyContent: 'center',
    alignItems: 'center'

  },
  title: {
    fontSize: 24,
    color: '#fff',
    fontWeight: '600'
  },
  box: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    fontSize: 18
  },
  inputContainer: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
    marginBottom: 100
  },
  input: {
    borderBottomWidth: 0.2,
    borderBottomColor: '#979797',
    flexDirection: 'row',
    width: '80%',
    fontSize: 18,
    paddingLeft: 20
  },
  message: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerButton: {
    alignItems: 'center'
  } ,
  button: {
    backgroundColor: '#00B528',
    width: 145,
    height: 45,
    backgroundColor: appStyle.mossColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5

  },
  textButton: {
    fontSize: 16,
    fontWeight: '500',
    color: appStyle.primaryColor
  }

});
