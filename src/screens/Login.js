import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Input from '../components/Input';
import { appStyle, container } from '../styles'
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { login } from '../store/actions/user';
import Message from '../components/Message';

const Login = ({route}) => {

    const navigation = useNavigation();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const { text } = useSelector(state => state.message);
    const dispatch = useDispatch();

    const validations = [];
    validations.push(email && email.includes('@'))
    validations.push(password && password.length >= 4)
    const [emailValid, passwordValid] = validations;
    const validForm = validations.reduce((all, v) => all && v)

    const onLogin = () => {
        dispatch(login(email, password)).then(() => {
            navigation.navigate("Home");
          
        }).catch(
            () => {
            //  dispatch(clearMessage())
            }
        )

    }
 

    return (
        <View style={container.style}>
        
            <View style={styles.titleContainer}>
                <Icon name="alarm" size={80} color={appStyle.mossColor} />
                <Text style={styles.title}>Registro Ponto</Text>
            </View>
            <View style={styles.inputs}>
                <Input name="email" label="Email" placeholder="Email" value={email} onChangeText={email => setEmail(email)} isValid={emailValid} />
                <Input name="password" label="Senha" placeholder="Senha" secure={true} value={password} onChangeText={password => setPassword(password)} isValid={passwordValid} />
            </View>
            <View style={styles.containerRegister}>
                <TouchableOpacity onPress={() => navigation.navigate('SignUp')} >
                    <Text style={styles.register}>Crie a sua conta</Text>
                </TouchableOpacity>
            </View>
            {text && text.trim() ? <Message text={text} /> : null}

            <TouchableOpacity onPress={onLogin}
                disabled={!validForm}
                style={[styles.button, validForm ? { backgroundColor: appStyle.mossColor } : {}]}>
                <Text style={styles.textButton}>Entrar</Text>
            </TouchableOpacity>
        </View>

    )
}



const styles = StyleSheet.create({
    inputs: {
        display: 'flex',
        marginTop: 20,
        marginBottom: 10

    },
    title: {
        fontSize: 40,
        fontWeight: '700',

    },
    titleContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 60,
    },
    containerRegister: {
        flexDirection: 'row',
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 25
    },
    register: {
        fontSize: 16,
        color: '#00B528',
        fontWeight: '600',
        textAlign: 'center',
        // justifyContent: 'space-evenly',
        left: 15,
        

    },
    button: {
        backgroundColor: appStyle.mossColor,
        padding: 10,
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10


    },
    textButton: {

        color: '#fff',
        fontSize: 19,
        fontWeight: '400'

    }

})

export default Login;

