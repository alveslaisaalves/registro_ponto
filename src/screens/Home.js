import React, { useEffect } from "react";
import { View, Text } from "react-native";
import { useNavigation } from '@react-navigation/native';
import Header from "../components/Header";
import Date from "../components/TodayDate";
import { useDispatch, useSelector } from "react-redux";
import RegisterPoint from "../components/RegisterPoint";

const Home = () => {

    const navigation = useNavigation();
    const { user } = useSelector(state => state.user);

    return (
        <>
            <Header today={<Date dateType="month" />} name={user.name} idUser={user.userId} />
            <View style={{backgroundColor:"#CCCDEFA"}}>
                <RegisterPoint />
            </View>

        </>


    )
}
export default Home;

