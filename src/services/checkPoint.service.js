
import axios from "axios";
import { API_URL } from "../config/api";

const checkPoint = (point, note) => {
    return axios.post(`${API_URL}/point`, {
             "point": point,
             "justificativa": note
     });
 
 };

const getCalcPoints = (idUser, date) => {
    return axios.get(`${API_URL}/user/points/${idUser}/calculo/${date}`).then(
        response => response.data
    )
}

 const getListPoints = (idUser, date) => {
    return axios.get(`${API_URL}/user/points/${idUser}/date/${date}`).then(
        response => response.data
    )
}
const getPoint = (idUser, date) => {
    return axios.get(`${API_URL}/user/points/${idUser}/point/${date}`).then(
        response => response.data
    )
}
 export default {
     checkPoint,
     getCalcPoints,
     getListPoints,
     getPoint
 }