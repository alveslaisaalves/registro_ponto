import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";
import { API_URL } from "../config/api";

const register = (name, email, password) => {
    return axios.post(`${API_URL}/users`, { "senha": password, "name": name, "email": email }).then(
        response =>  response
        
    );
};

const login = (email, password) => {
    return axios.post(`${API_URL}/login?username=${email}&password=${password}`).then(
        response => {
            if (response.data) {
                AsyncStorage.setItem("@user", JSON.stringify(response.data));
            }
          
            return response.data;
            
        }
    )       
} 

const logout = () => { 
    AsyncStorage.removeItem("@user");
}

const update = (id, name, email, password) => {
    return axios.put(`${API_URL}/users`, {
        "idUser": id,
        "name": name,
        "email": email,
        "senha": password
    }).then(response => response)
}

export default {
    login,
    logout,
    register,
    update
}