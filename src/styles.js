import {StyleSheet} from "react-native";

export const container = StyleSheet.create({
    style: {
        display: "flex",
        flexDirection: "column",
        flexWrap: "nowrap",
        alignItems: "center",
        marginHorizontal: 10,
    }
})

export const appStyle = StyleSheet.create({
    primaryColor: "#fff",
    secundaryColor: "#134E5E",
    mossColor: "#71B354"
})