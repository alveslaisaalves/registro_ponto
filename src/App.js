import React from "react";
import {
  SafeAreaView,
  Text
} from "react-native";
import { NavigationContainer, useNavigationState } from '@react-navigation/native';

import Routes from "./routes/index.js";
import { Provider } from "react-redux"
import configStore from "./store/storeConfig";
import { useDispatch, useSelector } from 'react-redux';
import { clearMessage } from "./store/actions/message.js";


const store = configStore();

const App = () => {
  const dispatch = useDispatch(); 
  return (
    <SafeAreaView style={{ flex: 1, fontSize: 40}}>
        <NavigationContainer onStateChange={(state) => dispatch(clearMessage())}>
          <Routes />
        </NavigationContainer>
    </SafeAreaView>
  );
};

const appWrapper = () => {
  return(
    <Provider store={store}>
      <App />
    </Provider>
  )
  
}

export default appWrapper;
